**CALENDAR-APP-VANILLA-JS**

---

You can look up into calendar and book appointment! This app is made completely with vanillaJS!
Implemented LC for storing Appointments Information...

---

**CALENDAR-PAGE**
![calendar-page](/images/calendar.png)

---

**APPOINTMENT-PAGE**
![Appointment-page](/images/appointments.png)

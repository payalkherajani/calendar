import { MonthMap, Utils } from "./utils.js"
import {  AppointmentUI } from "./appointment.js"

//Calendar Class
export class Calendar {

    static getCurrentMonth(){
        return new Date().getMonth()
    }
    static getCurrentYear(){
         return new Date().getFullYear()
    }

    static getTodayDate(){
      return new Date().getDate()
    }

    static getTotalNumberOfDaysInGivenMonth(month,year){
        return new Date(year, month + 1,0).getDate()    //month + 1 if not done, returns previous month number of days
    }

    static getFirstDayOfGivenMonthAndYear(month,year){
        return new Date(year, month).getDay()
    }

    static renderCalendar(month,year,hightlightDate){
        const daysInGivenMonth = this.getTotalNumberOfDaysInGivenMonth(month,year)
        const firstDayNum = this.getFirstDayOfGivenMonthAndYear(month,year) //gives number, see in DayMap[this.getFirstDayOfGivenMonthAndYear(month,year)]
        const dateToHighlight = Calendar.getTodayDate()
        let date=1
        for (let row = 1; row<=6; row++){

            let weekRow = UI.createRow()
            weekRow.classList.add(`week-${row}`)

            for(let col = 0; col< 7; col++){
                if(row === 1 && col < firstDayNum){
                    const tableData = UI.createTD()
                    tableData.innerHTML = ''
                     weekRow.appendChild(tableData)
                }
                else if(date > daysInGivenMonth){
                    break
                }
                else{
                    const tableData = UI.createTD()
                    if(hightlightDate && dateToHighlight === date){
                        tableData.classList.add('today',`date-${date}-${month}-${year}`,'bg-info','text-center')
                        tableData.style.cursor = "pointer";
                    }
                    tableData.classList.add('text-center', `date-${date}-${month}-${year}`)
                    tableData.style.cursor = "pointer"
                    tableData.innerHTML = `${date}`
                    weekRow.appendChild(tableData)
                    date++
                    tableData.addEventListener('mouseover',function(){
                        this.style.backgroundColor = 'pink'
                    }) 
        
                    tableData.addEventListener('mouseleave', function() {
                        this.style.backgroundColor = ""
                    })
                }
            }
           
            UI.appendRowInTable(weekRow)
            weekRow?.addEventListener('click', (e) => {
                const arrayOfClasses = e.target.classList[1].split('-')
                AppointmentUI.openModal()
                const date = (arrayOfClasses[3]) + '-' + (arrayOfClasses[2] < 9 ?  '0' + arrayOfClasses[2] : arrayOfClasses[2]) + '-' + (arrayOfClasses[1] < 9 ?  '0' + arrayOfClasses[1]: arrayOfClasses[1])
                AppointmentUI.renderFormWithDate(date)
            })
           
        }
        
    }
}

//UI Class

export class UI {

    static onLoadRenderCalendarBasedOnUserMachineDate(){
        const currentMonth = Calendar.getCurrentMonth()
        const currentYear =  Calendar.getCurrentYear()
        UI.displayMonthAndYear(MonthMap[currentMonth],currentYear)
        Calendar.renderCalendar(currentMonth,currentYear, true)
    }

    static displayMonthAndYear(currMonth,currYear){
        const monthAndYearSpan = document.querySelector('#current-month')
        monthAndYearSpan.innerText = `${currMonth}-${currYear}`
    }

    static createRow(){
        return document.createElement('tr')
    }

    static createTD(){
        return document.createElement('td')
    }

    static appendRowInTable(row){
        const tableBody = document.querySelector('#calendar-table-body')
        tableBody.appendChild(row)
    }

    static clearTableBody() {
        const tableBody = document.querySelector('#calendar-table-body')
        tableBody.innerHTML = ''
    }

    static clearDisplayMonthAndYear(){
        const monthAndYearSpan = document.querySelector('#current-month')
        monthAndYearSpan.innerHTML = ' '
    }

    static renderCalendarBasedOnNextOrPreviousBtn(nextMonth,nextYear,highlightTodayDate){
        UI.clearDisplayMonthAndYear()
        UI.displayMonthAndYear(MonthMap[nextMonth],nextYear)
        UI.clearTableBody()
        Calendar.renderCalendar(nextMonth,nextYear,highlightTodayDate)
    }

}

//Event Listeners

//next btn
document.querySelector('#next-month')?.addEventListener('click', () => {
    const { displayMonth,displayYear } = Utils.getDisplayedMonthAndYearInSpan()
    const { nextMonth, nextYear, highlightTodayDate } = Utils.getNextMonthAndYear(displayMonth,displayYear)
    UI.renderCalendarBasedOnNextOrPreviousBtn(nextMonth,nextYear,highlightTodayDate)
})


//previous month
document.querySelector('#previous-month')?.addEventListener('click',() => {
    const { displayMonth,displayYear } = Utils.getDisplayedMonthAndYearInSpan()
    const { nextMonth, nextYear, highlightTodayDate } = Utils.getPreviousMonthAndYear(displayMonth,displayYear)
    UI.renderCalendarBasedOnNextOrPreviousBtn(nextMonth,nextYear,highlightTodayDate)
})




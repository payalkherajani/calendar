import { UI} from './calendar.js'

//On DOM load => load calendar
document.addEventListener('DOMContentLoaded', UI.onLoadRenderCalendarBasedOnUserMachineDate)

document.querySelector('#calendar-page')?.addEventListener('click', (e) => {
     e.preventDefault()
     document.querySelector('#appointment-nav-item').style.textDecoration='none'
     document.querySelector('#calendar-nav-item').style.textDecoration='underline'
     document.querySelector('#calendar-container').style.display = 'block'
     document.querySelector('#appointment-container').style.display = 'none'
   
})


document.querySelector('#appointment-page')?.addEventListener('click', (e) => {
    e.preventDefault()
    document.querySelector('#calendar-nav-item').style.textDecoration='none'
    document.querySelector('#appointment-nav-item').style.textDecoration='underline'
    document.querySelector('#appointment-container').style.display = 'block'
    document.querySelector('#calendar-container').style.display = 'none'
})
export class Appointment{
   constructor(date,customerName, service){
    this.date = date
    this.customerName = customerName
    this.service = service
   }
}


export class AppointmentUI{

    static renderFormWithDate(date){
       document.querySelector('#appointment-date').value=date
    }

    static showAlert(msg, className) {
        const alertDiv = document.createElement('div')
        alertDiv.className = `alert alert-${className} m-4`
        alertDiv.appendChild(document.createTextNode(msg))
        const container = document.querySelector('.container')
        const modal = document.querySelector('.modal')
        container.insertBefore(alertDiv, modal)

        //Remove Alert after 3sec

        setTimeout(() => {
            document.querySelector('.alert').remove()
        }, 3000)
    }

    static addAppointmentToList(appointment){
        const list = document.querySelector('#appointments-table-body')
        const row = document.createElement('tr')
        row.innerHTML = `
         <td class="text-center">${appointment.date}</td>
         <td class="text-center">${appointment.customerName}</td>
         <td class="text-center">${appointment.service}</td>
         <td class="text-center"><a href="#" class="btn btn-danger btn-sm delete">X</a></td>
        `
        list.appendChild(row)
    }

    static clearAppointmentFields(){
        document.querySelector('#appointment-date').value = ''
        document.querySelector('#customer-name').value = ''
        document.querySelector('#service').value = ''

    }

    static displayAppointments() {
        const StoredAppointments = Store.getAppointments()
        const appointments = StoredAppointments       
        appointments.forEach((appointment) => AppointmentUI.addAppointmentToList(appointment))
    }

    static deleteAppointment(el) {
        if (el.classList.contains('delete')) {
            el.parentElement.parentElement.remove()
            AppointmentUI.showAlert("Appointment Cancelled", 'success')
        }
    }

    static openModal(){
        const modal = document.querySelector('#appointment-modal')
        modal.style.display = 'initial'
        const customerName = document.querySelector('#customer-name')
        customerName.focus()
    }

    static closeModal(){
        document.querySelector('#appointment-modal').style.display = 'none'
    }


}

class Store{
    static getAppointments() {

        let appointments

        if (localStorage.getItem('appointments') === null) {
            appointments = []
        } else {
            appointments = JSON.parse(localStorage.getItem('appointments'))
        }
        return appointments
    }

    static addAppointment(appointment) {
        const appointments = Store.getAppointments()
        appointments.push(appointment)
        localStorage.setItem('appointments', JSON.stringify(appointments))
    }

    static removeAppointment(date) {
        const appointments = Store.getAppointments()
        appointments.forEach((appointment, i) => {
            if (appointment.date === date) {
                appointments.splice(i, 1)
            }
        })
        localStorage.setItem('appointments', JSON.stringify(appointments))
    }
}

document.addEventListener('DOMContentLoaded', AppointmentUI.displayAppointments)

//event listener for submitting form

document.querySelector('#appointment-save-btn').addEventListener('click', (e) => {
    e.preventDefault()
    const date = document.querySelector('#appointment-date').value
    const customerName = document.querySelector('#customer-name').value
    const service = document.querySelector('#service').value

    if(date === '' || customerName === '' || service === ''){
        AppointmentUI.showAlert("Please Fill all fields", 'danger')
    }
    else{
        const newAppointment = new Appointment(date,customerName,service)
        AppointmentUI.addAppointmentToList(newAppointment)
        Store.addAppointment(newAppointment)
        AppointmentUI.showAlert("Appointment Booked Successfully", 'info')
        AppointmentUI.clearAppointmentFields()
        AppointmentUI.closeModal()
    }
})



//event listeners

//delete appointment
document.querySelector('#appointments-table-body').addEventListener('click', (e) => {
     AppointmentUI.deleteAppointment(e.target)
     Store.removeAppointment(e.target.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.textContent)
})

//close appointment dialog event (2 close btns, therefore used querySelectorAll)
document.querySelectorAll('#appointment-close-btn').forEach((e) => {
    e.addEventListener('click', () => {
        AppointmentUI.closeModal()
    })
})

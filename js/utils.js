import { Calendar, UI }from './calendar.js'

export const MonthMap = {
    0: 'January',
    1: 'Febuary',
    2: 'March',
    3: 'April',
    4: 'May',
    5: 'June',
    6: 'July',
    7: 'August',
    8: 'September',
    9: 'October',
    10: 'November',
    11: 'December'
}

export const DayMap = {
    0: 'Sun',
    1: 'Mon',
    2: 'Tue',
    3: 'Wed',
    4: 'Thur',
    5: 'Fri',
    6: 'Sat'
}

export class Utils{

    static getNextMonthAndYear(displayMonth,displayYear){
        let nextMonth, nextYear,highlightTodayDate
        const currentMonth = Calendar.getCurrentMonth()
        const currentYear =  Calendar.getCurrentYear()
    
        if(displayMonth === 11){
            nextYear = displayYear + 1
            nextMonth = 0
        }
        else{
            nextMonth = displayMonth + 1
            nextYear = displayYear
        }
    
        if(currentMonth === nextMonth && currentYear === nextYear){
            highlightTodayDate = true
        }else{
            highlightTodayDate = false
        }
        return { nextMonth,nextYear, highlightTodayDate }
    }

    static getPreviousMonthAndYear(displayMonth,displayYear){
        let nextMonth, nextYear,highlightTodayDate
        const currentMonth = Calendar.getCurrentMonth()
        const currentYear =  Calendar.getCurrentYear()

        if(displayMonth === 0){
            nextYear = displayYear - 1
            nextMonth = 11
        }
        else{
            nextMonth = displayMonth - 1
            nextYear = displayYear
        }
        
        if(currentMonth === nextMonth && currentYear === nextYear){
            highlightTodayDate = true
        }else{
            highlightTodayDate = false
        }
        return { nextMonth,nextYear, highlightTodayDate}
    }

    static getDisplayedMonthAndYearInSpan(){
        const displayMonthAndYear = document.querySelector('#current-month').innerText.split('-')
        let displayMonth = Number(Object.keys(MonthMap)[Object.values(MonthMap).indexOf(displayMonthAndYear[0])])
        let displayYear = Number(displayMonthAndYear[1])
        return { displayMonth,displayYear }
    }
}